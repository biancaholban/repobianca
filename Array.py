def construct(n, m, a):
    ind = 0

    for i in range(n):
        if (a[i] != -1):
            ind = i
            break

    for i in range(ind - 1, -1, -1):
        if (a[i] == -1):
            a[i] = (a[i + 1] - 1 + m) % m

    for i in range(ind + 1, n):
        if (a[i] == -1):
            a[i] = (a[i - 1] + 1) % m
    print(*a)


n, m = 6, 7
a = [5, -1, -1, 9, 2, 3]
construct(n, m, a)