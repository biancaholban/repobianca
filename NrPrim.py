
def prim(n):
    divizori = 0
    d = 1
    while d <= n:
        if n % d == 0:
            divizori += 1
        if divizori == 3:
            return
        d += 1
    if divizori == 2:
        print(str(n) + " este prim!")
    else:
        print(str(n) + " nu este prim!")

def add(num1,num2):
    sum = float(num1) + float(num2)
# Display the sum
    print('The sum of {0} and {1} is {2}'.format(num1, num2, sum))