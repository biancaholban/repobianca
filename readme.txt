Act as if what you do makes a difference. It does.— William James

What counts in life is not the mere fact that we have lived.
It is what difference we have made to the lives of others that
will determine the significance of the life we lead.— Nelson Mandela

I have one life and one chance to make it count for something…
My faith demands that I do whatever I can, wherever I am, whenever
I can, for as long as I can with whatever I have to try to make
a difference.— Jimmy Carter