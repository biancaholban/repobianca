my_list = ['internship', 'NTT', 'IT', 'summer',
           'program', 'exception', 'smile', 'love',
           'questions', 'words', 'life']

def divide_chunks(l, n):
    for i in range(0, len(l), n):
        yield l[i:i + n]

n = 5

x = list(divide_chunks(my_list, n))
print(x)