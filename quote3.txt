In the end, it's not the years in your life that count. It's the life in your years.
Abraham Lincoln

Update1
�Twenty years from now you will be more disappointed by the things that you didn�t do than by the ones you did do.�
-Mark Twain

Update2
�You only live once, but if you do it right, once is enough. � -Mae West

Update3
�Let us always meet each other with smile, for the smile is the beginning of love.�
-Mother Theresa

Update4
�Challenges are what make life interesting and overcoming them is what makes life meaningful.�
-Joshua J. Marine

Update5
�A friend is someone who gives you total freedom to be yourself.�
-Jim Morrison

Update6
�Every great dream begins with a dreamer. Always remember, you have within you the strength, the patience, and the passion to reach for the stars to change the world.�
-Harriet Tubman