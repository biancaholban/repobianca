def Convert(tup, di):
    for a, b in tup:
        di.setdefault(a, []).append(b)
    return di


tups = [("NTT", 10), ("Endava", 12), ("MSG", 14),
        ("EBS", 20), ("Arobs", 25), ("ISDC", 30)]
dictionary = {}
print(Convert(tups, dictionary))