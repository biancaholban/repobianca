test_dict = {"Internship": 22, "NTT": 21, "Summer": 21, "IT": 21}

print("The dictionary before performing remove is : " + str(test_dict))

del test_dict['Summer']

print("The dictionary after remove is : " + str(test_dict))

del test_dict['IT']