Life is inherently risky. There is only one big risk you should avoid at all costs, 
and that is the risk of doing nothing.

Denis Waitley


Update
Your time is limited, so don't waste it living someone else's life.

Steve Jobs